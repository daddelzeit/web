# DaddelzeitWeb

## Container operations

### Containerize the app

Run the following command from the root directory (same as this file):

```bash
podman build -t daddelzeit-web -f assets/container/Containerfile .
```

The Containerfile uses dependency caching, so unless the dependencies change only the app packages themselves require compilation.

### Running the app container

Run the container:

```bash
podman run -d --name daddelzeit-web --publish 127.0.0.1:8080:8080/tcp codeberg.org/buzz-tee/daddelzeit-web:latest
```

If everything went as it should you can point your browser to http://localhost:8080/daddelzeit and start using the app.

### Configuration parameters

| **Environment variable** | **Description** | **Default value** |
|--------------------------|-----------------|-------------------|
| API_HOST | The hostname and port on which the API is running.  | daddelzeit-api:8080 |
| APP_ROOT | The root path on which to expose the web interface<br>Will result in http(s)://host${APP_ROOT}/, be sure to have APP_ROOT begin with a '/' | /daddelzeit |

If left unchanged you must ensure that the API container is running as daddelzeit-api and that the web container is able to resolve the name otherwise the container will crash.
Name resolution usually requires that both containers run on the same container network with DNS enabled. You may want to assign fixed IP addresses to your containers and pass that as API_HOST.

### SSL/TLS

The app container does **not** offer TLS termination. It is strongly recommended to use a reverse proxy to provide secure communication.

## Development

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 17.0.7.

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
