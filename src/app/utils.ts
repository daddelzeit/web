import { AbstractControl, ValidationErrors } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { NotificationType, NotificationComponent, NotificationData } from "./components/notification/notification.component";

const DEFAULT_SNACKBAR_DURATION = 3;

export function extendValidationError(control: AbstractControl, error: ValidationErrors) {
    if (control.errors === null) {
        control.setErrors(error);
    } else {
        Object.assign(control.errors, error);
    }
}

export function removeValidationError(control: AbstractControl, error: ValidationErrors) {
    if (control.errors !== null) {
        Object.keys(error)
            .forEach((key) => { delete control.errors![key]; })
        if (!Object.keys(control.errors).length) {
            control.setErrors(null);
        }
    }
}

export function showInfo(snackBar: MatSnackBar, message: string, meta?: string, timeoutInSeconds?: number) {
    snackBar.openFromComponent(NotificationComponent, {
        duration: (timeoutInSeconds ?? DEFAULT_SNACKBAR_DURATION) * 1000,
        data: <NotificationData>{
            type: NotificationType.INFO,
            message,
            meta,
        }
    });
}

function getStatusText(response: Response) {
    switch(response.status) {
        case 400: return $localize `:@@errors.bad-request:Request could not be processed`;
        case 401: return $localize `:@@errors.unauthorized:Unauthorized request`;
        case 403: return $localize `:@@errors.forbidden:Operation is not allowed`;
        case 404: return $localize `:@@errors.not-found:Not found`;
        case 500: return $localize `:@@errors.server-error:Server error`;
        default: return response.statusText;
    }
}

export function showError(snackBar: MatSnackBar, message: string, cause: unknown, timeoutInSeconds?: number) {
    const meta = (cause instanceof Response)
        ? getStatusText(cause)
        : (cause instanceof Error)
        ? cause.message
        : String(cause);

    snackBar.openFromComponent(NotificationComponent, {
        duration: (timeoutInSeconds ?? DEFAULT_SNACKBAR_DURATION) * 1000,
        data: <NotificationData>{
            type: NotificationType.ERROR,
            message,
            meta,
        }
    });
}
