import { Routes } from '@angular/router';

import { OverviewComponent } from './components/overview/overview.component';

// import { ActivitiesComponent as ActivitiesComponent } from './components/activities/activities.component';
// import { TrendComponent } from './modules/child/components/trend/trend.component';
// import { ChildComponent } from './components/child/child.component';
import { LoginComponent } from './components/login/login.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';

export const routes: Routes = [
    { path: '', component: OverviewComponent, pathMatch: 'full' },
    { path: 'login', component: LoginComponent, title: 'Anmelden' },
    { path: 'password', component: ChangePasswordComponent, title: 'Passwort ändern' },
    { path: 'activity-templates', loadChildren: () => import('./modules/activity-templates/activity-templates.module').then((m) => m.ActivityTemplatesModule) },
    { path: ':child', loadChildren: () => import('./modules/child/child.module').then((m) => m.ChildModule) },
    { path: '**', redirectTo: '' },
];
