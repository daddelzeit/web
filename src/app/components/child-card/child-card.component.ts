import { Component, Input, inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { RouterModule } from '@angular/router';
import { ChildAmountDto } from '../../models/child';
import { ActivityDto, ActivityTemplateDto } from '../../models/activity';
import { ApiService } from '../../services/api.service';
import { EMPTY, catchError, map, shareReplay, switchMap, take } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { LogActivityComponent, LogActivityDialogData } from '../log-activity/log-activity.component';
import { OneResponse } from '../../models/response';
import { MatSnackBar } from '@angular/material/snack-bar';
import { showError } from '../../utils';
import { CommonModule } from '@angular/common';
import { BreakpointObserver } from '@angular/cdk/layout';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Component({
  selector: 'app-child-card',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatDividerModule,
    MatProgressSpinnerModule,
  ],
  templateUrl: './child-card.component.html',
  styleUrl: './child-card.component.scss'
})
export class ChildCardComponent {

  protected readonly NEGATIVE = (activity: ActivityTemplateDto) => {
    return activity.amount <= 0;
  }
  protected readonly POSITIVE = (activity: ActivityTemplateDto) => {
    return activity.amount >= 0;
  }

  protected readonly isSmallScreen$;

  private apiService = inject(ApiService);
  private dialog = inject(MatDialog);
  private snackBar = inject(MatSnackBar);

  protected child_: ChildAmountDto|null = null;
  protected hideActions_: boolean = true;
  protected hideLinks_: boolean = false;

  @Input()
  set child(child: ChildAmountDto|null) {
    this.child_ = child;
  }

  @Input()
  set hideActions(hideActions: boolean) {
    this.hideActions_ = hideActions;
  }

  @Input()
  set hideLinks(hideLinks: boolean) {
    this.hideLinks_ = hideLinks;
  }

  constructor (
    breakpointObserver: BreakpointObserver,
  ) {
    this.isSmallScreen$ = breakpointObserver.observe([
        '(max-width: 340px)'
      ])
      .pipe(
        takeUntilDestroyed(),
        map((result) => result.matches),
        shareReplay(),
      )
  }

  protected logActivity(filter?: (activity: ActivityTemplateDto) => boolean) {
    this.apiService.getActivityTemplates()
      .pipe(
        take(1),
        switchMap((response) => {
          const activityTemplates = response.results
            .filter(filter ?? (() => true));

          const dialogRef = this.dialog.open(LogActivityComponent, {
            data: <LogActivityDialogData> {
              activityTemplates,
            }
          });

          return dialogRef.afterClosed();
        }),
        catchError((e) => {
          showError(this.snackBar, $localize `:@@child.load-templates-error:Could not load activity templates`, e);
          return EMPTY;
        }),
        switchMap((activity?: ActivityTemplateDto) => {
          if (!activity || !this.child_) {
            return EMPTY;
          }

          return this.apiService.createActivity(
            this.child_.name, activity.description, activity.amount);
        }),
        switchMap((activity?: OneResponse<ActivityDto>) => {
          if (!activity || !this.child_) {
            return EMPTY;
          }

          return this.apiService.getChild(this.child_?.name);
        })
      )
      .subscribe({
        next: (response?: OneResponse<ChildAmountDto>) => {
          if (response) {
            this.child_ = response.result;
          }
        },
        error: (e) => {
          showError(this.snackBar, $localize `:@@child.log-error:Failed to log activity`, e);
        }
      });
  }
}
