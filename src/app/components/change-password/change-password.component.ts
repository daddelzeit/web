import { Component, inject } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ReactiveFormsModule, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { ApiService } from '../../services/api.service';
import { NavigationService } from '../../services/navigation.service';
import { take } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { extendValidationError, removeValidationError, showError, showInfo } from '../../utils';
import { MatDividerModule } from '@angular/material/divider';

@Component({
  selector: 'app-change-password',
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatDividerModule,
  ],
  templateUrl: './change-password.component.html',
  styleUrl: './change-password.component.scss'
})
export class ChangePasswordComponent {

  private readonly apiService = inject(ApiService);
  private readonly navigationService = inject(NavigationService);
  private readonly snackBar = inject(MatSnackBar);

  private readonly passwordMatchValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    const newPassword = this.changePasswordForm?.controls.newPassword;
    const newPasswordConfirm = this.changePasswordForm?.controls.newPasswordConfirm;
    if (!newPassword || !newPasswordConfirm ||
        newPassword.pristine || newPasswordConfirm.pristine) {
      return null;
    }

    const passwordsNotMatchingError = {
      passwordsNotMatching: true,
    };
    const passwordsNotMatching = newPassword.value !== newPasswordConfirm.value;
    const otherControl = control === newPassword
      ? newPasswordConfirm
      : newPassword;

    if (otherControl.touched) {
      control.markAsTouched();
    }
    
    if (passwordsNotMatching) {
      extendValidationError(otherControl, passwordsNotMatchingError);
      return passwordsNotMatchingError;
    } else {
      removeValidationError(otherControl, passwordsNotMatchingError);
      return null;
    }
  }

  protected hidePassword = true;
  protected hideNewPassword = true;
  protected hideNewPasswordConfirm = true;
  protected changePasswordForm = new FormGroup({
    password: new FormControl('', [
      Validators.required,
    ]),
    newPassword: new FormControl('', [
      Validators.required,
      this.passwordMatchValidator,
    ]),
    newPasswordConfirm: new FormControl('', [
      Validators.required,
      this.passwordMatchValidator,
    ]),
  });

  protected changePassword() {
    this.apiService.changePassword(
      this.changePasswordForm.controls.password.value!,
      this.changePasswordForm.controls.newPassword.value!,
    )
    .pipe(take(1))
    .subscribe({
      next: (x) => {
        showInfo(this.snackBar, $localize `:@@change-password.success:Password changed successfully`);
        this.changePasswordForm.reset();
        this.navigationService.goHome();
      },
      error: (e) => {
        showError(this.snackBar, $localize `:@@change-password.failed:Failed to change password`, e);
      }
    })
  }
}
