import { Component, inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { AuthenticationService } from '../../services/authentication.service';
import { ApiService } from '../../services/api.service';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { NavigationService } from '../../services/navigation.service';
import { take } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { showError } from '../../utils';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    ReactiveFormsModule,
  ],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent {
  protected authenticationService = inject(AuthenticationService);
  private apiService = inject(ApiService);
  private navigationService = inject(NavigationService);
  private snackBar = inject(MatSnackBar);

  protected hide = true;
  protected loginForm = new FormGroup({
    username: new FormControl('', [
      Validators.required,
    ]),
    password: new FormControl('', [
      Validators.required,
    ]),
  });

  protected togglePassword() {
    this.hide = !this.hide;
    return false;
  }

  protected login() {
    const username = this.loginForm.controls.username.value ?? '';
    const password = this.loginForm.controls.password.value ?? '';
    
    this.apiService.login(username, password)
      .pipe(take(1))
      .subscribe({
        next: (response) => {
          this.authenticationService.authenticated(response.result);
          this.navigationService.goBack();
        },
        error: (e) => {
          showError(this.snackBar, $localize `:@@login.error:Login failed`, e);
        }
      });
  }

  protected onEnter(event: KeyboardEvent) {
    if (event.key === 'Enter') {
      event.preventDefault();
      this.login();
    }
  }
}
