import { Component, inject } from '@angular/core';
import { Observable, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { NavigationService } from '../services/navigation.service';
import { ApplicationRoles, AuthenticationService } from '../services/authentication.service';
import { TemplatePageTitleStrategy } from '../strategies/template-page-title.strategy';

interface AuthenticationData {
  isAuthenticated: boolean;
  initials?: string;
  roleIcon?: string;
  name?: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent {
  protected readonly navigationService = inject(NavigationService);
  protected readonly titleStrategy = inject(TemplatePageTitleStrategy);

  protected readonly authenticationData$: Observable<AuthenticationData>;

  constructor(
    private authenticationService: AuthenticationService,
  ) {
    const getInitials = (name: string|undefined) => {
      const parts = name?.split(' ') ?? [];
      if (parts.length > 1) {
        return parts[0][0] + parts[0][1];
      }
      if (parts.length == 1) {
        return parts[0].substring(0, 3);
      }
      return undefined;
    }
    const getRoleIcon = (roles: ApplicationRoles[]|undefined) => {
      if (roles?.includes(ApplicationRoles.PARENT)) {
        return 'account_circle';
      }
      if (roles?.includes(ApplicationRoles.CHILD)) {
        return 'supervised_user_circle';
      }
      return undefined;
    }
    this.authenticationData$ = combineLatest([
      authenticationService.isAuthenticated,
      authenticationService.getApplicationData()
    ])
    .pipe(
      map(([isAuthenticated, appData]) => {
        return <AuthenticationData> {
          isAuthenticated,
          initials: getInitials(appData?.name),
          roleIcon: getRoleIcon(appData?.roles),
          name: appData?.name,
        };
      })
    );
  }

  protected logout() {
    this.authenticationService.logout();
    this.navigationService.goHome();
  }
}
