import { Component, DestroyRef, Inject, OnInit, inject } from '@angular/core';
import { MatAutocompleteModule, MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ActivityTemplateDto } from '../../models/activity';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { AbstractControl, FormControl, FormGroup, ReactiveFormsModule, ValidatorFn, Validators } from '@angular/forms';
import { Observable, map, startWith } from 'rxjs';
import { AsyncPipe } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { extendValidationError, removeValidationError } from '../../utils';

export interface LogActivityDialogData {
  activityTemplates: ActivityTemplateDto[],
}

@Component({
  selector: 'app-log-activity',
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatAutocompleteModule,
    MatDialogModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatCheckboxModule,
    ReactiveFormsModule,
    AsyncPipe,
  ],
  templateUrl: './log-activity.component.html',
  styleUrl: './log-activity.component.scss'
})
export class LogActivityComponent implements OnInit {

  private readonly validateConfirmZero: ValidatorFn = (control: AbstractControl) => {
    const amount = this.logActivityForm?.controls.amount;
    const confirmZero = this.logActivityForm?.controls.confirmZero;
    if (!amount || !confirmZero) {
      return null;
    }

    const unconfirmedZeroError = {
      unconfirmedZero: true,
    };
    const unconfirmedZero = (amount.value ?? 0) === 0 && !(confirmZero.value ?? false);
    const otherControl = control === amount ? confirmZero : amount;

    if (unconfirmedZero) {
      extendValidationError(otherControl, unconfirmedZeroError);
      return unconfirmedZeroError;
    } else {
      removeValidationError(otherControl, unconfirmedZeroError);
      return null;
    }
  }

  protected logActivityForm = new FormGroup({
    description: new FormControl('', [
      Validators.required,
    ]),
    amount: new FormControl(0, [
      Validators.required,
      this.validateConfirmZero,
    ]),
    confirmZero: new FormControl(false, [
      this.validateConfirmZero,
    ])
  })

  protected filteredOptions$?: Observable<ActivityTemplateDto[]>;
  private destroyRef = inject(DestroyRef);

  constructor(
    protected dialogRef: MatDialogRef<LogActivityComponent>,
    @Inject(MAT_DIALOG_DATA) protected data: LogActivityDialogData,
  ) {
    console.log("Dialog data:", data.activityTemplates);
  }

  protected displayFn(activityTemplate: ActivityTemplateDto): string {
    return activityTemplate?.description || '';
  }

  protected onSelected(event: MatAutocompleteSelectedEvent) {
    const activityTemplate = event.option.value as ActivityTemplateDto;
    this.logActivityForm.controls.amount.setValue(activityTemplate?.amount ?? 0);
  }

  protected getActivity(): ActivityTemplateDto {
    const descriptionValue: unknown|null = this.logActivityForm.controls.description.value;

    return <ActivityTemplateDto>{
      description: (descriptionValue instanceof ActivityTemplateDto)
        ? descriptionValue.description
        : descriptionValue ?? '',
      amount: this.logActivityForm.controls.amount.value ?? 0,
    };
  }

  protected clearDescription() {
    this.logActivityForm.controls.description.setValue(null);
  }

  protected incAmount() {
    const value = this.logActivityForm.controls.amount.value ?? -1;
    this.logActivityForm.controls.amount.setValue(value + 1);
  }

  protected decAmount() {
    const value = this.logActivityForm.controls.amount.value ?? 1;
    this.logActivityForm.controls.amount.setValue(value - 1);
  }

  ngOnInit(): void {
    this.filteredOptions$ = this.logActivityForm.controls.description.valueChanges
      .pipe(
        startWith(''),
        map((value: string | null) => {
          return this.data.activityTemplates
            .filter((activityTemplate) => activityTemplate.description.toLowerCase()
              .includes(value || ''));
        })
      );
    this.logActivityForm.controls.amount.valueChanges
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (value) => {
          if (value) {
            this.logActivityForm.controls.confirmZero.setValue(false);
          }
        }
      })
  }
}
