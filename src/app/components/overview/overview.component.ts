import { Component } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { ChildAmountDto } from '../../models/child';
import { Observable } from 'rxjs';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ChildCardComponent } from '../child-card/child-card.component';
import { AuthenticationService } from '../../services/authentication.service';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { ManyResponse } from '../../models/response';

@Component({
  selector: 'app-overview',
  standalone: true,
  imports: [
    CommonModule,
    ChildCardComponent,
    MatCardModule,
    MatButtonModule,
    RouterModule,
    MatProgressSpinnerModule,
  ],
  templateUrl: './overview.component.html',
  styleUrl: './overview.component.scss'
})
export class OverviewComponent {

  protected children$: Observable<ChildAmountDto[]>;
  protected isParent$: Observable<boolean>;
  
  constructor(
    protected authenticationService: AuthenticationService,
    apiService: ApiService,
  ) {
    this.isParent$ = authenticationService.getApplicationData()
      .pipe(
        AuthenticationService.mapIsParent(),
        takeUntilDestroyed(),
      );
    this.children$ = apiService.getChildren()
      .pipe(
        ManyResponse.mapResults(),
        takeUntilDestroyed(),
      )
  }
}
