import { map } from "rxjs";

export enum ResponseStatus {
    Success,
    Error,
}

export class ManyResponseWithContext<C,T> {
    status!: ResponseStatus;
    context!: C;
    results!: T[];
}

export class ManyResponse<T> {
    status!: ResponseStatus;
    results!: T[];

    public static mapResults<T>() {
        return map((response: ManyResponse<T>) => response.results);
    }
}

export class OneResponseWithContext<C,T> {
    status!: ResponseStatus;
    context!: C;
    result!: T;
}

export class OneResponse<T> {
    status!: ResponseStatus;
    result!: T;

    public static mapResult<T>() {
        return map((response: OneResponse<T>) => response.result);
    }
}

export class ErrorResponse {
    status!: ResponseStatus;
    message!: string;
}