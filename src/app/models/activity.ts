import { Type } from "class-transformer";
import { ParentDto } from "./parent";

import 'reflect-metadata';

export class ActivityDto {
    id!: number;
    @Type(() => ParentDto)
    parent?: ParentDto;
    description?: string;
    amount!: number;
    @Type(() => Date)
    timestamp!: Date;
}

export class ActivityRequestDto {
    description!: string;
    amount!: number;
}

export class ActivityTemplateDto {
    id!: number;
    description!: string;
    amount!: number;
}

export class ActivityTemplateRequestDto {
    description!: string;
    amount!: number;
}