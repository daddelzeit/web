export class AuthenticationRequestDto {
    username!: string;
    password!: string;
}

export class RefreshAuthenticationRequestDto {
    refreshToken!: string;
}

export class AuthenticationDto {
    accessToken!: string;
    refreshToken!: string;
}

export class ChangePasswordRequestDto {
    password!: string;
    newPassword!: string;
}