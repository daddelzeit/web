import { Type } from "class-transformer";

import 'reflect-metadata';

export class TrendDto {
    @Type(() => Date)
    date!: Date;
    balance!: number;
}
