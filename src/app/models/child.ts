export class ChildDto {
    name!: string;
    displayName!: string;
    baseAmount!: number;
}

export class ChildAmountDto {
    name!: string;
    displayName!: string;
    baseAmount!: number;
    balance?: number;
}
