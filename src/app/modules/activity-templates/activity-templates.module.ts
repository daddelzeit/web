import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, provideRouter, withComponentInputBinding } from '@angular/router';
import { ActivityTemplatesListComponent } from './components/activity-templates-list/activity-templates-list.component';

const routes: Routes = [
  { path: '', component: ActivityTemplatesListComponent, title: 'Aktivitäten' },
];

@NgModule({
  providers: [
    provideRouter(routes, withComponentInputBinding()),
  ],
  declarations: [],
  imports: [
    CommonModule,
  ]
})
export class ActivityTemplatesModule { }
