import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityTemplateDialogComponent } from './activity-template-dialog.component';

describe('ActivityTemplateDialogComponent', () => {
  let component: ActivityTemplateDialogComponent;
  let fixture: ComponentFixture<ActivityTemplateDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ActivityTemplateDialogComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ActivityTemplateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
