import { Component, Inject } from '@angular/core';
import { ActivityTemplateDto } from '../../../../models/activity';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { AsyncPipe } from '@angular/common';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';

export interface ActivityTemplateDialogData {
  activityTemplate?: ActivityTemplateDto;
}

@Component({
  selector: 'app-activity-template-dialog',
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatDialogModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatCheckboxModule,
    ReactiveFormsModule,
    AsyncPipe,
  ],
  templateUrl: './activity-template-dialog.component.html',
  styleUrl: './activity-template-dialog.component.scss'
})
export class ActivityTemplateDialogComponent {

  protected wantDelete = false;

  protected activityTemplateForm = new FormGroup({
    description: new FormControl('', [
      Validators.required,
    ]),
    amount: new FormControl(0, [
      Validators.required,
    ]),
  })

  constructor(
    protected dialogRef: MatDialogRef<ActivityTemplateDialogComponent>,
    @Inject(MAT_DIALOG_DATA) protected data: ActivityTemplateDialogData,
  ) {
    if (data.activityTemplate) {
      this.activityTemplateForm.controls.description.setValue(data.activityTemplate.description);
      this.activityTemplateForm.controls.amount.setValue(data.activityTemplate.amount);
    }
  }

  protected getActivityTemplate(): ActivityTemplateDto {
    return <ActivityTemplateDto>{
      id: this.data.activityTemplate?.id,
      description: this.activityTemplateForm.controls.description.value ?? '',
      amount: this.activityTemplateForm.controls.amount.value ?? 0,
    };
  }

  protected incAmount() {
    const value = this.activityTemplateForm.controls.amount.value ?? -1;
    this.activityTemplateForm.controls.amount.setValue(value + 1);
  }

  protected decAmount() {
    const value = this.activityTemplateForm.controls.amount.value ?? 1;
    this.activityTemplateForm.controls.amount.setValue(value - 1);
  }
}
