import { AfterContentInit, Component, inject } from '@angular/core';
import { MatListModule } from '@angular/material/list';
import { ApiService } from '../../../../services/api.service';
import { EMPTY, Observable, map, switchMap, take } from 'rxjs';
import { ActivityTemplateDto } from '../../../../models/activity';
import { AsyncPipe, CommonModule } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { ActivityTemplateDialogComponent, ActivityTemplateDialogData } from '../activity-template-dialog/activity-template-dialog.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBar } from '@angular/material/snack-bar';
import { showError } from '../../../../utils';
import { AuthenticationService } from '../../../../services/authentication.service';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Component({
  selector: 'app-activity-templates-list',
  standalone: true,
  imports: [
    AsyncPipe,
    CommonModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
  ],
  templateUrl: './activity-templates-list.component.html',
  styleUrl: './activity-templates-list.component.scss'
})
export class ActivityTemplatesListComponent implements AfterContentInit {

  private apiService = inject(ApiService);
  private dialog = inject(MatDialog);
  private snackBar = inject(MatSnackBar);

  protected activityTemplates$: Observable<ActivityTemplateDto[]> = EMPTY;
  protected isParent$: Observable<boolean>;

  constructor(
    authenticationService: AuthenticationService,
  ) {
    this.isParent$ = authenticationService.getApplicationData()
      .pipe(
        AuthenticationService.mapIsParent(),
        takeUntilDestroyed(),
      );
  }

  private loadActivityTemplates() {
    this.activityTemplates$ = this.apiService.getActivityTemplates()
      .pipe(
        take(1),
        map((result) => result.results.sort((a, b) => a.description.localeCompare(b.description)))
      );
  }

  ngAfterContentInit(): void {
    this.loadActivityTemplates();
  }

  protected activityTemplateClicked(activityTemplate?: ActivityTemplateDto) {
    const dialogRef = this.dialog.open(ActivityTemplateDialogComponent, {
      data: <ActivityTemplateDialogData> {
        activityTemplate: activityTemplate,
      }
    });
    dialogRef.afterClosed()
      .pipe(
        switchMap((dialogResult: ActivityTemplateDto|null) => {
          if (activityTemplate) {
            if (dialogResult) {
              return this.apiService.updateActivityTemplate(dialogResult);
            } else if (dialogResult === null) {
              return this.apiService.deleteActivityTemplate(activityTemplate);
            }
          } else {
            if (dialogResult) {
              return this.apiService.createActivityTemplate(dialogResult.description, dialogResult.amount);
            }
          }
          return EMPTY;
        })
      )
      .subscribe({
        next: () => {
          this.loadActivityTemplates();
        },
        error: (e) => {
          showError(this.snackBar, $localize `:@@activity-templates-list.operation-failed:Operation failed`, e);
        }
      })
  }

}
