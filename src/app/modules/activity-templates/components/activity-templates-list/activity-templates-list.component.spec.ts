import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityTemplatesListComponent } from './activity-templates-list.component';

describe('DefaultActivitiesListComponent', () => {
  let component: ActivityTemplatesListComponent;
  let fixture: ComponentFixture<ActivityTemplatesListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ActivityTemplatesListComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ActivityTemplatesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
