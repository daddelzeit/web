import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { Routes, provideRouter, withComponentInputBinding } from '@angular/router';
import { TrendComponent } from './components/trend/trend.component';
import { ChildViewComponent } from './components/child-view/child-view.component';
import { ActivitiesComponent } from './components/activities/activities.component';

const routes: Routes = [
  { path: 'activities', component: ActivitiesComponent, title: 'Verlauf' },
  { path: 'trend', component: TrendComponent, title: 'Trend' },
  { path: '', component: ChildViewComponent, title: ChildViewComponent.resolveTitle },
]

@NgModule({
  providers: [
    provideRouter(routes, withComponentInputBinding()),
    DatePipe,
  ],
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class ChildModule { }
