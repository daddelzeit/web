import { Component, DestroyRef, Input, inject } from '@angular/core';
import { ActivityDto } from '../../../../models/activity';
import { ApiService } from '../../../../services/api.service';
import { ActivityListComponent } from '../activity-list/activity-list.component';
import { ChildCardComponent } from '../../../../components/child-card/child-card.component';
import { ChildAmountDto } from '../../../../models/child';
import { Subject, map } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { AsyncPipe } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBar } from '@angular/material/snack-bar';
import { showError } from '../../../../utils';

interface ChildData {
  child: ChildAmountDto|null;
  activities: ActivityDto[];
}

@Component({
  selector: 'app-activities',
  standalone: true,
  imports: [
    ChildCardComponent,
    ActivityListComponent,
    MatCardModule,
    MatProgressSpinnerModule,
    AsyncPipe,
  ],
  templateUrl: './activities.component.html',
  styleUrl: './activities.component.scss'
})
export class ActivitiesComponent {

  private apiService = inject(ApiService);
  private destroyRef = inject(DestroyRef);
  private snackBar = inject(MatSnackBar);

  protected childName?: string;
  protected childData$: Subject<ChildData> = new Subject();

  @Input()
  set child(childName: string) {
    this.childName = childName;

    this.apiService.getActivities(childName)
      .pipe(
        map((result) => ({
          child: result.context,
          activities: result.results,
        })),
        takeUntilDestroyed(this.destroyRef),
      )
      .subscribe({
        next: (result) => {
          this.childData$.next(result);
        },
        error: (e) => {
          showError(this.snackBar, $localize `:@@activities.load-failed:Could not load history`, e);
          this.childData$.next({
            child: null,
            activities: [],
          });
        }
      })
  }

}
