import { Component, Input } from '@angular/core';
import { MatListModule } from '@angular/material/list';
import { ActivityDto } from '../../../../models/activity';
import { CommonModule } from '@angular/common';
import {CdkScrollableModule} from '@angular/cdk/scrolling';

@Component({
  selector: 'app-activity-list',
  standalone: true,
  imports: [
    CommonModule,
    MatListModule,
    CdkScrollableModule,
  ],
  templateUrl: './activity-list.component.html',
  styleUrl: './activity-list.component.scss'
})
export class ActivityListComponent {

  protected activities_: ActivityDto[] = [];

  @Input()
  set activities(activities: ActivityDto[]) {
    this.activities_ = activities;
  }

}
