import { Component, Input, inject } from '@angular/core';
import { ChildAmountDto } from '../../../../models/child';
import { TrendDto } from '../../../../models/trend';
import { ApiService } from '../../../../services/api.service';
import { ChildCardComponent } from '../../../../components/child-card/child-card.component';
import { MatListModule } from '@angular/material/list';
import { CommonModule, DatePipe } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';

import { BarControllerChartOptions, Chart, ChartDataset, Color, CoreChartOptions, DatasetChartOptions, ElementChartOptions, PluginChartOptions, ScaleChartOptions, ScriptableScaleContext } from 'chart.js';
import { NgChartsModule } from 'ng2-charts';
import { _DeepPartialObject } from 'chart.js/dist/types/utils';

interface TrendCoordinates {
  x: string;
  y: number;
}
interface ChartConfig {
  labels: string[];
  datasets: ChartDataset<"bar", TrendCoordinates[]>[];
  options: _DeepPartialObject<CoreChartOptions<"bar"> & ElementChartOptions<"bar"> & PluginChartOptions<"bar"> & DatasetChartOptions<"bar"> & ScaleChartOptions<"bar"> & BarControllerChartOptions>;
}

@Component({
  selector: 'app-trend',
  standalone: true,
  imports: [
    CommonModule,
    ChildCardComponent,
    MatIconModule,
    MatListModule,
    NgChartsModule,
  ],
  templateUrl: './trend.component.html',
  styleUrl: './trend.component.scss'
})
export class TrendComponent {
  private apiService = inject(ApiService);
  private datePipe = inject(DatePipe);

  protected childName?: string;

  protected chart_: ChartConfig|null = null;

  protected child_: ChildAmountDto|null = null;
  protected trend_: TrendDto[] = [];

  @Input()
  set child(childName: string) {
    const toShortDateString = (date: Date) => this.datePipe.transform(date, 'shortDate') ?? date.getTime().toString();

    this.childName = childName;

    this.apiService.getTrend(childName)
      .subscribe({
        next: (result) => {
          this.child_ = result.context;
          this.trend_ = result.results;

          const labels = [];
          if (result.results.length) {
            for (let cur = new Date(result.results[result.results.length - 1].date); cur < new Date(); cur.setDate(cur.getDate() + 1)) {
              labels.push(toShortDateString(cur));
            }
          }

          this.chart_ = {
            labels,
            datasets: [
              {
                data: result.results.map((trend) => ({
                  x: toShortDateString(trend.date), y: trend.balance
                })),
                borderWidth: 2,
                borderRadius: 5,
              }
            ],
            options: {
              maintainAspectRatio: false,
              plugins: {
                legend: {
                  display: false
                },
              },
              scales: {
                y: {
                  grid: {
                    color: (ctx: ScriptableScaleContext) => {
                      if (ctx.tick.value === 0) {
                        return '#404040';
                      } else {
                        return <Color>Chart.defaults.borderColor;
                      }
                    }
                  }
                }
              }
            },
          };
        }
      })
  }
}
