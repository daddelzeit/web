import { Component, DestroyRef, Input, inject } from '@angular/core';
import { ApiService } from '../../../../services/api.service';
import { ChildAmountDto } from '../../../../models/child';
import { ActivityDto } from '../../../../models/activity';
import { ActivatedRouteSnapshot } from '@angular/router';
import { ChildCardComponent } from '../../../../components/child-card/child-card.component';
import { ActivityListComponent } from '../activity-list/activity-list.component';
import { AuthenticationService } from '../../../../services/authentication.service';
import { EMPTY, Observable, Subject, map, switchMap, takeUntil } from 'rxjs';
import { AsyncPipe } from '@angular/common';
import { TemplatePageTitleStrategy } from '../../../../strategies/template-page-title.strategy';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { MatCardModule } from '@angular/material/card';
import { MatProgressSpinner, MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBar } from '@angular/material/snack-bar';
import { showError } from '../../../../utils';

interface ChildData {
  child: ChildAmountDto|null;
  activities: ActivityDto[]|null;
}

@Component({
  selector: 'app-child-view',
  standalone: true,
  imports: [
    ChildCardComponent,
    ActivityListComponent,
    MatCardModule,
    MatProgressSpinnerModule,
    AsyncPipe,
  ],
  templateUrl: './child-view.component.html',
  styleUrl: './child-view.component.scss'
})
export class ChildViewComponent {

  public static readonly DISABLE_BACK = true;

  private apiService = inject(ApiService);
  private destroyRef = inject(DestroyRef);
  private snackBar = inject(MatSnackBar);

  private titleStrategy = inject(TemplatePageTitleStrategy);

  protected childData$: Subject<ChildData> = new Subject();
  protected isParent$: Observable<boolean>;

  static resolveTitle(route: ActivatedRouteSnapshot) {
    const child = route.params['child'];
    return Promise.resolve(child[0].toUpperCase() + child.substring(1));
  }

  constructor(
    private authenticationService: AuthenticationService,
  ) {
    this.isParent$ = authenticationService.getApplicationData()
      .pipe(
        AuthenticationService.mapIsParent(),
        takeUntilDestroyed(),
      );
  }

  @Input()
  set child(childName: string) {
    this.authenticationService.isAuthenticated
      .pipe(
        switchMap((authenticated) => {
          if (authenticated) {
            return this.apiService.getActivities(childName)
              .pipe(
                map((results) => ({
                    child: results.context,
                    activities: results.results,
                  })
                )
              )
          } else {
            return this.apiService.getChild(childName)
              .pipe(
                map((result) => ({
                    child: result.result,
                    activities: [],
                  })
                )
              )
          }
        }),
        takeUntilDestroyed(this.destroyRef),
      )
      .subscribe({
        next: (childData) => {
          this.titleStrategy.updateSubtitle(childData.child.displayName);
          this.childData$.next(childData);
        },
        error: (e) => {
          showError(this.snackBar, $localize `:@@child.load-failed:Could not load view`, e);
          this.childData$.next({
            child: null,
            activities: null,
          })
        },
      });
  }
}
