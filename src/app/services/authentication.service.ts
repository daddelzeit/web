import { Injectable } from '@angular/core';
import { JwtPayload, jwtDecode } from 'jwt-decode';
import { BehaviorSubject, Observable, catchError, map, of, shareReplay, switchMap } from 'rxjs';
import { AuthenticationDto } from '../models/authentication';
import { ApiService } from './api.service';

export enum ApplicationRoles {
  PARENT,
  CHILD,
}

export interface ApplicationData {
  name: string,
  roles: ApplicationRoles[],
}

interface DaddelzeitAccessToken extends JwtPayload {
  app: ApplicationData,
  jwt: string,
}

interface DaddelzeitRefreshToken extends JwtPayload {

}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private static readonly ACCESS_TOKEN = "accessToken";
  private static readonly REFRESH_TOKEN = "refreshToken";

  /// Refresh token after 95% of its lifetime, should eliminate any clock skew
  private static readonly TOKEN_EXPIRY_SAFETY_MARGIN = 0.05;

  private readonly refreshAccessToken$ = new BehaviorSubject<void>(undefined);
  private readonly accessToken$: Observable<DaddelzeitAccessToken|null>;

  constructor() {
    this.accessToken$ = this.refreshAccessToken$
      .pipe(
        map(() => {
          const accessToken = localStorage.getItem(AuthenticationService.ACCESS_TOKEN);
          const payload = this.decodeToken<DaddelzeitAccessToken>(accessToken);
          if (!!payload && !!accessToken) {
            payload.app = <ApplicationData> {
              name: payload.app.name,
              roles: payload.app.roles
                .map((role: unknown) => (typeof role === 'string' || role instanceof String) ? ApplicationRoles[role.toUpperCase() as keyof typeof ApplicationRoles] : <ApplicationRoles>role)
            }
            payload.jwt = accessToken;
          }
          return payload;
        }),
        shareReplay(1),
      );
  }

  public static mapIsParent() {
    return map((appData: ApplicationData | undefined) => appData?.roles.includes(ApplicationRoles.PARENT) ?? false);
  }

  private decodeToken<T>(token: string | null) {
    const accessToken = token;
    if (accessToken) {
      return jwtDecode<T>(accessToken);
    }
    return null;
  }

  private verify<T extends JwtPayload>(token: T | null) {
    if (!!token && token.exp) {
      if (token.iat) {
        const lifetime = token.exp - token.iat;
        const adjustedExpiry = token.iat + lifetime * (1 - AuthenticationService.TOKEN_EXPIRY_SAFETY_MARGIN);
        return adjustedExpiry > (Date.now() / 1000);
      } else {
        return token.exp > (Date.now() / 1000);
      }
    }
    return false;
  }

  public get isAuthenticated() {
    return this.accessToken$
      .pipe(
        map(this.verify),
        map((result) => {
          if (!result) {
            const refreshToken = localStorage.getItem(AuthenticationService.REFRESH_TOKEN);
            if (refreshToken) {
              const decoded = jwtDecode(refreshToken);
              return this.verify(decoded);
            }
          }
          return result;
        })
      );
  }

  public getApplicationData() {
    return this.accessToken$
      .pipe(
        map((accessToken) => accessToken?.app)
      );
  }

  public authenticated(data: AuthenticationDto) {
    this.setRefreshToken(data.refreshToken);
    this.setAccessToken(data.accessToken);
  }

  public logout() {
    this.clearRefreshToken();
    this.clearAccessToken();
  }

  private clearRefreshToken() {
    localStorage.removeItem(AuthenticationService.REFRESH_TOKEN);
  }

  private setRefreshToken(value: string) {
    localStorage.setItem(AuthenticationService.REFRESH_TOKEN, value);
  }

  private clearAccessToken() {
    localStorage.removeItem(AuthenticationService.ACCESS_TOKEN);
    this.refreshAccessToken$.next();
  }

  private setAccessToken(value: string) {
    localStorage.setItem(AuthenticationService.ACCESS_TOKEN, value);
    this.refreshAccessToken$.next();
  }

  public authenticateHeaders(headers: { [key: string]: string }, apiService: ApiService) {
    return this.accessToken$
      .pipe(
        switchMap((accessToken) => {
          if (!!accessToken && this.verify(accessToken)) {
            headers['Authorization'] = `Bearer ${accessToken.jwt}`;
          } else {
            const refreshToken = localStorage.getItem(AuthenticationService.REFRESH_TOKEN);
  
            const decoded = this.decodeToken<DaddelzeitRefreshToken>(refreshToken);
            if (refreshToken && this.verify(decoded)) {
              return apiService.refreshAuthentication(refreshToken)
                .pipe(
                  map((result) => {

                    this.setRefreshToken(result.result.refreshToken);
                    this.setAccessToken(result.result.accessToken);

                    headers['Authorization'] = `Bearer ${result.result.accessToken}`;
                    return headers;
                  }),
                  catchError(() => {
                    return of(headers);
                  })
                );
            } else {
              if (refreshToken) this.clearRefreshToken();
              if (accessToken) this.clearAccessToken();
            }
          }
          return of(headers);
        })
      );
  }
}
