import { Injectable, inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, map, of, shareReplay, switchMap } from 'rxjs';
import { LoginComponent } from '../components/login/login.component';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  private route = inject(ActivatedRoute);
  private router = inject(Router);

  get isBackAvailable$(): Observable<boolean> {
    return of(this.route.firstChild)
      .pipe(
        map((child) => {
          const children = [];
          while (child) {
            children.push(child);
            child = child.firstChild;
          }
          return children;
        }),
        switchMap((children) => {
          const disableBack = children.find((child) => {
            return child.component
              && ('DISABLE_BACK' in child.component)
              && !!child.component.DISABLE_BACK;
          });
          if (disableBack) {
            return of(false);
          }

          return this.route.firstChild
            ?.url
            ?.pipe(
              map((url) => url.length > 0),
              shareReplay(1),
            ) ?? of(false);
        })
      )
  }

  get isLoginComponent(): boolean {
    return this.route.firstChild
      ?.component === LoginComponent
  }

  goBack(): void {
    this.router.navigateByUrl("/");
  }

  goHome(): void {
    this.router.navigateByUrl("/");
  }
}
