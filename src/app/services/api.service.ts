import { Injectable, inject } from '@angular/core';
import { EMPTY, Observable, OperatorFunction, catchError, from, map, mergeMap, of, switchMap, take, throwError } from 'rxjs';
import { fromFetch } from 'rxjs/fetch';
import { ChildAmountDto, ChildDto } from '../models/child';
import { ManyResponse, ManyResponseWithContext, OneResponse, OneResponseWithContext, ResponseStatus } from '../models/response';
import { ActivityDto, ActivityRequestDto, ActivityTemplateDto, ActivityTemplateRequestDto } from '../models/activity';
import { TrendDto } from '../models/trend';
import { ClassConstructor, plainToClass } from 'class-transformer';
import { AuthenticationService } from './authentication.service';
import { AuthenticationDto, AuthenticationRequestDto, ChangePasswordRequestDto, RefreshAuthenticationRequestDto } from '../models/authentication';
import { NavigationService } from './navigation.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { showError } from '../utils';
import { environment } from '../../environments/environment';

interface RequestOptions<T> {
  jsonResponse?: boolean;
  failureMessage?: string;
  authenticated?: boolean;
  method?: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  payload?: any;       // payload will be passed to JSON.stringify()
  mapFn?: OperatorFunction<T, T>;
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private authenticationService = inject(AuthenticationService);
  private navigationService = inject(NavigationService);
  private snackBar = inject(MatSnackBar);

  private static readonly API_PREFIX =
    environment.apiRoot && !environment.apiRoot.endsWith('/')
    ? `${environment.apiRoot}/api`
    : `${environment.apiRoot}api`;

  private handleResponse<T>(response: Response, json: boolean, redirect401: boolean, failureMessage: string | undefined): Observable<T> {
    if (response.status < 400) {
      if (json) {
        return from(response.json());
      } else {
        return from(response.text())
          .pipe(
            map(v => v as T)
          );
      }
    } else {
      console.log('Request failed, response status', response.status, failureMessage);

      if (response.status === 401 && redirect401) {
        this.navigationService.goHome();
        return EMPTY;
      } else {
        return throwError(() => response);
      }
    }
  }

  private toStatus(status: unknown): ResponseStatus {
    return (typeof status === 'string' || status instanceof String) ? ResponseStatus[status.toUpperCase() as keyof typeof ResponseStatus] : <ResponseStatus>status;
  }

  private mapMany<T>(clsResult: ClassConstructor<T>): OperatorFunction<ManyResponse<T>, ManyResponse<T>> {
    return map((response) => <ManyResponse<T>>{
      status: this.toStatus(response.status),
      results: response.results.map((result) => plainToClass(clsResult, result)),
    });
  }

  private mapManyWithContext<C, T>(clsContext: ClassConstructor<C>, clsResult: ClassConstructor<T>)
  : OperatorFunction<ManyResponseWithContext<C, T>, ManyResponseWithContext<C, T>> {
    return map((response) => <ManyResponseWithContext<C, T>>{
      status: this.toStatus(response.status),
      context: plainToClass(clsContext, response.context),
      results: response.results.map((result) => plainToClass(clsResult, result)),
    });
  }

  private mapOne<T>(clsResult: ClassConstructor<T>): OperatorFunction<OneResponse<T>, OneResponse<T>> {
    return map((response) => <OneResponse<T>>{
      status: this.toStatus(response.status),
      result: plainToClass(clsResult, response.result)
    });
  }

  private mapOneWithContext<C, T>(clsContext: ClassConstructor<C>, clsResult: ClassConstructor<T>)
  : OperatorFunction<OneResponseWithContext<C, T>, OneResponseWithContext<C, T>> {
    return map((response) => <OneResponseWithContext<C, T>>{
      status: this.toStatus(response.status),
      context: plainToClass(clsContext, response.context),
      result: plainToClass(clsResult, response.result)
    });
  }

  private request<T>(path: string, options?: RequestOptions<T>): Observable<T> {
    const initHeaders: { [key: string]: string } = {}
    if (options?.payload) {
      initHeaders['Content-Type'] = 'application/json';
    }
    const headers = (options?.authenticated ?? true)
      ? this.authenticationService.authenticateHeaders(initHeaders, this)
      : of(initHeaders);
    return headers
      .pipe(
        switchMap((headers) => fromFetch(`${ApiService.API_PREFIX}/${path}`, {
          method: options?.method ?? 'GET',
          headers: headers,
          body: options?.payload ? JSON.stringify(options.payload) : null,
        })),
        mergeMap(response => this.handleResponse<T>(response, options?.jsonResponse ?? true, options?.authenticated ?? true, options?.failureMessage)),
        options?.mapFn ?? map((value) => value),
        map((response) => {
          console.debug(`API request ${options?.method == 'GET'} ${path} returned response`, response);
          return response;
        }),
        catchError((e) => {
          console.error(`API request ${options?.method == 'GET'} ${path} returned error`, e);
          if (options?.failureMessage) {
            showError(this.snackBar, options?.failureMessage, e);
          }
          return throwError(() => e);
        }),
        take(1),
      );
  }

  public getChildren() {
    return this.request('v1/children',
      {
        mapFn: this.mapMany(ChildAmountDto),
      });
  }

  public getChild(child_name: string) {
    return this.request(`v1/children/${child_name}`,
      {
        mapFn: this.mapOne(ChildAmountDto),
      });
  }

  public getActivities(child_name: string) {
    return this.request(`v1/children/${child_name}/activities`,
      {
        mapFn: this.mapManyWithContext(ChildAmountDto, ActivityDto),
      });
  }

  public createActivity(child_name: string, description: string, amount: number) {
    return this.request(`v1/children/${child_name}/activities`,
      {
        mapFn: this.mapOneWithContext(ChildDto, ActivityDto),
        method: 'POST',
        payload: <ActivityRequestDto>{
          description,
          amount,
        },
      });
  }

  public getActivityTemplates() {
    return this.request('v1/activities/templates',
      {
        mapFn: this.mapMany(ActivityTemplateDto),
      });
  }

  public createActivityTemplate(description: string, amount: number) {
    return this.request('v1/activities/templates',
      {
        mapFn: this.mapOne(ActivityTemplateDto),
        method: 'POST',
        payload: <ActivityTemplateRequestDto>{
          description,
          amount,
        },
      });
  }

  public updateActivityTemplate(activityTemplate: ActivityTemplateDto) {
    return this.request(`v1/activities/templates/${activityTemplate.id}`,
      {
        mapFn: this.mapOne(ActivityTemplateDto),
        method: 'PUT',
        payload: <ActivityTemplateRequestDto>{
          description: activityTemplate.description,
          amount: activityTemplate.amount,
        },
      });
  }

  public deleteActivityTemplate(activityTemplate: ActivityTemplateDto) {
    return this.request(`v1/activities/templates/${activityTemplate.id}`,
      {
        jsonResponse: false,
        method: 'DELETE',
      });
  }


  public getTrend(child_name: string) {
    return this.request(`v1/children/${child_name}/trend`,
      { 
        mapFn: this.mapManyWithContext(ChildAmountDto, TrendDto),
      });
  }

  public login(username: string, password: string) {
    return this.request('v1/auth',
      {
        mapFn: this.mapOne(AuthenticationDto),
        authenticated: false,
        method: 'POST',
        payload: <AuthenticationRequestDto>{
          username, password,
        },
      });
  }

  public refreshAuthentication(refreshToken: string) {
    return this.request('v1/auth',
      {
        mapFn: this.mapOne(AuthenticationDto),
        authenticated: false,
        method: 'PATCH',
        payload: <RefreshAuthenticationRequestDto> {
          refreshToken,
        }, 
      })
  }

  public changePassword(password: string, newPassword: string) {
    return this.request('v1/auth',
      { 
        jsonResponse: false,
        method: 'PUT',
        payload: <ChangePasswordRequestDto> {
          password, newPassword,
        }, 
      })
  }
}
