import { NgModule } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { AppComponent } from './components/app.component';
import { RouterModule, RouterOutlet, TitleStrategy, provideRouter, withComponentInputBinding } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { routes } from './app.routes';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { TemplatePageTitleStrategy } from './strategies/template-page-title.strategy';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterOutlet,
    RouterModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
  ],
  providers: [
    provideRouter(routes, withComponentInputBinding()),
    {
        provide: TitleStrategy,
        useClass: TemplatePageTitleStrategy,
    },
],
  bootstrap: [
    AppComponent,
  ]
})
export class AppModule {
  constructor() {
  }  
}
