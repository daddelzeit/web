import { Injectable } from "@angular/core";
import { Title } from "@angular/platform-browser";
import { RouterStateSnapshot, TitleStrategy } from "@angular/router";

@Injectable({providedIn: 'root'})
export class TemplatePageTitleStrategy extends TitleStrategy {
    constructor(private readonly _title: Title) {
        super();
    }
    
    public get title() {
        return this._title.getTitle();
    }

    public updateSubtitle(subtitle?: string) {
        const title = subtitle ? `Daddelzeit - ${subtitle}` : 'Daddelzeit';
        this._title.setTitle(title);
    }

    override updateTitle(snapshot: RouterStateSnapshot): void {
        const subTitle = this.buildTitle(snapshot);
        this.updateSubtitle(subTitle);
    }
}
