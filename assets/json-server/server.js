const jwtDecode = require('jwt-decode');
const jwtEncode = require('jwt-encode');
const jsonServer = require('json-server');
const server = jsonServer.create();
const router = jsonServer.router('db.json');
const middlewares = jsonServer.defaults({
    logger: false,
});

const API_BASE_URL = '/api';
const JWT_SECRET = 'secret';
const JWT_ACCESS_LIFETIME = 20;
const JWT_REFRESH_LIFETIME = 3600;
const DEFAULT_SERVER_LISTEN_PORT = 8001;

var serverListenPort = DEFAULT_SERVER_LISTEN_PORT;

const filenameArg = process.argv
    .find((value) => value.endsWith(__filename));
const paramsStart = filenameArg ? (process.argv.indexOf(filenameArg) + 1) : 0;
const params = process.argv.slice(paramsStart).reverse();
while (params.length > 0) {
    const param = params.pop();
    const value = param.startsWith('-')
        && params.length
        && !params[params.length - 1].startsWith('-')
        ? params.pop() : null;

    switch (param) {
        case '-p':
        case '--port': {
            if (value) {
                serverListenPort = value;
            }
        }
    }
}

console.log(`Starting up on port ${serverListenPort}`);

server.use(jsonServer.bodyParser);
server.use((req, res, next) => {
    const start = performance.now();
    const oldSend = res.send;
    const authUser = parseAuthUser(req);
    req.authUser = authUser;
    
    res.send = (body) => {
        const duration = performance.now() - start;
        console.log(req.method, req.originalUrl, res.statusCode, `${duration.toFixed(3)} ms`, ':', authUser?.username ?? 'anonymous');
        res.send = oldSend;
        return oldSend.call(res, body);
    }
    next();
});

function getChildResponse(child, withBalance=true) {
    if (withBalance) {
        const balance = router.db.get('activities')
            .filter((activity) => activity.child_id === child.id)
            .value()
            .reduce((accumulator, activity) => accumulator + activity.amount, 0);

        return {
            name: child.username,
            displayName: child.name,
            baseAmount: child.baseAmount,
            balance: balance ?? 0
        };    
    } else {
        return {
            name: child.username,
            displayName: child.name,
            baseAmount: child.baseAmount,
        };
    }
}

function getUserResponse(user) {
    return {
        name: user.name
    }
}

function getActivityResponse(activity) {
    const user = router.db.get('users')
        .find({id: activity.user_id})
        .value();

    return {
        id: activity.id,
        parent: user ? getUserResponse(user) : null,
        description: activity.description,
        amount: activity.amount,
        timestamp: new Date(activity.timestamp)
    };
}

function getChildByName(childName, res) {
    const child = router.db.get('children')
        .find((child) => child.username.toLowerCase() === childName.toLowerCase())
        .value();
    if (child) {
        return child;
    } else {
        if (res) {
            res.status(404)
                .json({
                    status: "Error",
                    message: "No such element"
                })
                .send();
        }
        return null;
    }
}

function getChildrenResponse(childName, res) {
    if (!!childName) {
        const child = getChildByName(childName, res);
        if (child) {
            return {
                status: "Success",
                result: getChildResponse(child)
            };
        } else {
            return null;
        }
    } else {
        const children = router.db.get('children')
            .value();
        return {
            status: "Success",
            results: children.map(child => getChildResponse(child))
        };
    }
}

function parseAuthUser(req) {
    const [_, token] = req.headers.authorization
        ?.split(' ', 2) ?? [null, null];

    if (!token) {
        return null;
    }

    try {
        const claims = jwtDecode.jwtDecode(token);
        if (!claims) {
            return null;
        }

        const authUser = router.db.get('users')
            .find({username: claims.sub})
            .value();
        return authUser;
    } catch (err) {
        return null;
    }
}

function getAuthUser(req, res) {
    const authUser = req.authUser;
    if (!authUser && res) {
        res.status(401)
            .send();
    }
    return authUser;
}

server.get(`${API_BASE_URL}/info`, (req, res) => {
    res.status(200)
        .json({
            'version': '0.1-mock'
        })
        .send();
})

server.post(`${API_BASE_URL}/v1/auth`, (req, res) => {
    const user = router.db.get('users')
        .find({username: req.body.username.toLowerCase(), password: req.body.password})
        .value();

    if (user) {

        const iat = Math.floor(Date.now() / 1000);

        const accessToken = {
            iss: 'Daddelzeit-Access',
            sub: user.username,
            iat,
            exp: iat + JWT_ACCESS_LIFETIME,
            app: {
                name: user.name,
                roles: user.roles,
            }
        }

        const refreshToken = {
            iss: 'Daddelzeit-Refresh',
            sub: user.username,
            iat,
            exp: iat + JWT_REFRESH_LIFETIME,
        }

        res.status(200)
            .send({
                "status": "Success",
                "result": {
                    accessToken: jwtEncode(accessToken, JWT_SECRET),
                    refreshToken: jwtEncode(refreshToken, JWT_SECRET),
                }
            });
    } else {
        res.status(401)
            .send();
    }
})

server.patch(`${API_BASE_URL}/v1/auth`, (req, res) => {
    const refreshToken = req.body.refreshToken;

    const claims = jwtDecode.jwtDecode(refreshToken);
    const user = router.db.get('users')
        .find({username: claims.sub})
        .value();

    if (!user) {
        res.status(400)
            .send();
        return;
    }

    const iat = Math.floor(Date.now() / 1000);

    const accessToken = {
        iss: 'Daddelzeit-Access',
        sub: user.username,
        iat,
        exp: iat + JWT_ACCESS_LIFETIME,
        app: {
            name: user.name,
            roles: user.roles,
        }
    }

    res.status(200)
    .send({
        "status": "Success",
        "result": {
            accessToken: jwtEncode(accessToken, JWT_SECRET),
            refreshToken: refreshToken,
        }
    });
})

server.put(`${API_BASE_URL}/v1/auth`, (req, res) => {
    const authUser = getAuthUser(req, res);
    if (!authUser) return;

    const user = router.db.get('users')
        .find({id: authUser.id, password: req.body.password})
        .value();

    if (!user) {
        res.status(400)
            .send();
        return;
    }

    user.password = req.body.newPassword;

    res.status(204)
        .send();
})

server.get(`${API_BASE_URL}/v1/children`, (req, res) => {
    if (!getAuthUser(req, res)) return;

    const children = getChildrenResponse();

    res.status(200)
        .send(children);
})

server.get(`${API_BASE_URL}/v1/children/:child`, (req, res) => {
    const child = getChildrenResponse(req.params.child, res);
    if (!child) return;

    res.status(200)
        .send(child);
});

server.get(`${API_BASE_URL}/v1/children/:child/activities`, (req, res) => {
    if (!getAuthUser(req, res)) return;

    const child = getChildByName(req.params.child, res);
    if (!child) return;

    const activities = router.db.get('activities')
        .filter({child_id: child.id})
        .orderBy('timestamp', 'desc')
        .value();

    res.status(200)
        .send({
            status: "Success",
            context: getChildResponse(child, true),
            results: activities.map(getActivityResponse)
        });
});

server.get(`${API_BASE_URL}/v1/children/:child/trend`, (req, res) => {
    if (!getAuthUser(req, res)) return;

    const child = getChildByName(req.params.child, res);
    if (!child) return;

    const activities = router.db.get('activities')
        .filter({child_id: child.id})
        .filter(activity => activity.user_id !== null)
        .orderBy('timestamp', 'desc')
        .value();

    const trend = activities.reduce((accumulator, activity) => {
        const date = new Date(activity.timestamp).toLocaleDateString();
        if (date in accumulator) {
            accumulator[date] += activity.amount;
        } else {
            accumulator[date] = activity.amount;
        }
        return accumulator;
    }, {});

    res.status(200)
        .send({
            status: "Success",
            context: getChildResponse(child, true),
            results: Object.keys(trend)
                .map((key) => ({
                    date: key,
                    balance: trend[key]
                }))
        });
});

server.post(`${API_BASE_URL}/v1/children/:child/activities`, (req, res) => {
    const authUser = getAuthUser(req, res);
    if (!authUser) return;

    const child = getChildByName(req.params.child, res);
    if (!child) return;

    const activities = router.db.get('activities')
        .value();

    const activity = {
        id: activities.reduce((a, b) => Math.max(a, b.id), 1) + 1,
        user_id: authUser.id,
        child_id: child.id,
        description: req.body.description,
        amount: req.body.amount,
        timestamp: new Date().toISOString()
    };
    
    activities.push(activity);
    
    res.redirect(303, `/api/v1/activities/${activity.id}`);
});

server.get(`${API_BASE_URL}/v1/activities/templates`, (req, res) => {
    const authUser = getAuthUser(req, res);
    if (!authUser) return;

    const activities = router.db.get('activity-templates')
        .value()
        .filter((v) => !!v);

    res.status(200)
        .send({
            status: "Success",
            results: activities,
        });
    });

server.get(`${API_BASE_URL}/v1/activities/templates/:id`, (req, res) => {
    const authUser = getAuthUser(req, res);
    if (!authUser) return;

    const activity = router.db.get('activity-templates')
        .find((activity) => activity.id === parseInt(req.params.id))
        .value();

    if (!activity) {
        res.status(404)
            .send();
        return;
    }

    res.status(200)
        .send({
            status: "Success",
            result: activity,
        });
});

server.post(`${API_BASE_URL}/v1/activities/templates`, (req, res) => {
    const authUser = getAuthUser(req, res);
    if (!authUser) return;

    const activities = router.db.get('activity-templates')
        .value();

    const activity = {
        id: activities.filter((v) => !!v).reduce((a, b) => Math.max(a, b.id), 1) + 1,
        description: req.body.description,
        amount: req.body.amount,
    };
    
    activities.push(activity);
    
    res.redirect(303, `/api/v1/activities/templates/${activity.id}`);
});

server.put(`${API_BASE_URL}/v1/activities/templates/:id`, (req, res) => {
    const authUser = getAuthUser(req, res);
    if (!authUser) return;

    const activity = router.db.get('activity-templates')
        .find((activity) => activity.id === parseInt(req.params.id))
        .value();

    if (!activity) {
        res.status(404)
            .send();
        return;
    }

    activity.description = req.body.description;
    activity.amount = req.body.amount;

    res.status(200)
        .send({
            status: "Success",
            result: activity,
        });
});

server.delete(`${API_BASE_URL}/v1/activities/templates/:id`, (req, res) => {
    const authUser = getAuthUser(req, res);
    if (!authUser) return;

    const activities = router.db.get('activity-templates')
        .value();

    const index = activities
        .filter((v) => !!v)
        .findIndex((activity) => activity.id === parseInt(req.params.id));

    if (index < 0) {
        res.status(404)
            .send();
        return;
    }

    delete activities[index];

    res.status(204)
        .send();
});

server.get(`${API_BASE_URL}/v1/activities/:id`, (req, res) => {
    if (!getAuthUser(req, res)) return;

    const activity = router.db.get('activities')
        .find({id: Number.parseInt(req.params.id)})
        .value();
    const child = router.db.get('children')
        .find({id: activity?.child_id})
        .value();
    
    if (!activity || !child) {
        res.status(404).send();
        return;
    }

    res.status(200)
        .send({
            status: "Success",
            context: getChildResponse(child),
            result: getActivityResponse(activity),
        });
});

server.use(middlewares);
server.use(router);

server.listen(serverListenPort, () => {
    console.log('JSON Server is ready');
    console.log();
})