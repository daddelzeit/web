FROM docker.io/node:lts-alpine AS builder

RUN apk --update add envsubst jq
RUN corepack enable

ARG APP_VERSION
ARG DEFAULT_LANGUAGE=en

RUN [ ! -z "${APP_VERSION}" ] || exit 1 ;\
    echo "Building daddelzeit-web ${APP_VERSION}"

WORKDIR /app

COPY . .

RUN jq '.projects.[].i18n.sourceLocale.baseHref = "${BASE_HREF}/" | .projects.[].i18n.locales.[].baseHref = "${BASE_HREF}/"' \
        < angular.json > angular.json.tmp \
    && mv angular.json.tmp angular.json

RUN envsubst < ./assets/container/templates/environment.ts > ./src/environments/environment.ts

RUN yarn install
RUN yarn run build

# Serve Application using Nginx Server
FROM docker.io/nginx:alpine-slim

ENV APP_ROOT=/daddelzeit
ENV API_HOST=http://daddelzeit-api:8080
ENV DEFAULT_LANGUAGE=${DEFAULT_LANGUAGE:-en}

COPY --from=builder /app/dist/daddelzeit-web/browser /app/html/
COPY ./assets/container/templates/default.conf /etc/nginx/templates/default.conf.template
COPY [ "./assets/container/entrypoint.d/16-resolver_override.envsh", \
       "./assets/container/entrypoint.d/17-languages.envsh", \
       "./assets/container/entrypoint.d/21-envsubst.sh", \
       "/docker-entrypoint.d/" ]
RUN echo "export DEFAULT_LANGUAGE=${DEFAULT_LANGUAGE}" >> /docker-entrypoint.d/17-languages.envsh
RUN chmod +x \
    /docker-entrypoint.d/16-resolver_override.envsh \
    /docker-entrypoint.d/17-languages.envsh \
    /docker-entrypoint.d/21-envsubst.sh

RUN mkdir -p /app/templates ; \
    for LANG_DIR in /app/html/* ; do \
    LANG=$(basename ${LANG_DIR}) ; \
    cp ${LANG_DIR}/index.html /app/templates/index.${LANG}.html ; \
    done

EXPOSE 8080
