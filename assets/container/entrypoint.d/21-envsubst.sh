#!/bin/sh

export BASE_HREF=$(echo ${APP_ROOT} | sed -e 's|^/*||' -e 's|/*$||')

for LANG_DIR in /app/html/*
do
    export LANG=$(basename ${LANG_DIR})

    envsubst < /app/templates/index.${LANG}.html > ${LANG_DIR}/index.html
done
